<?php

require_once ('animal.php');
require_once ('frog.php');
require_once ('ape.php');

$sheep = new animal('shaun');
echo "<h3>output akhir</h3>";
echo "Name : ". $sheep->name. "<br>"; // "shaun"
echo "legs : ". $sheep->legs. "<br>"; // 4
echo "cold blooded : ". $sheep->cold_blooded. "<br><br>"; // "no"

$kodok = new frog('buduk');
echo "Name : ". $kodok->name. "<br>"; // "buduk"
echo "legs : ". $kodok->legs. "<br>"; // 4
echo "cold blooded : ". $kodok->cold_blooded. "<br>"; // "no"
echo "Jump : ". $kodok->jump. "<br><br>"; // "Hop Hop"

$sungokong = new ape('kera sakti');
echo "Name : ". $sungokong->name. "<br>"; // "kera sakti"
echo "legs : ". $sungokong->legs. "<br>"; // 2
echo "cold blooded : ". $sungokong->cold_blooded. "<br>"; // "no"
echo "Yell : ". $sungokong->yell. "<br><br>"; // "Auooo"

?>